# Задание 3

Журнал

```
Script started on 2021-09-14 14:57:14+00:00 [TERM="tmux-256color" TTY="/dev/pts/0" COLUMNS="65" LINES="32"]
yanush13@vm1:~$ cat >examples/books
Book one.
The second book.
The third.
THis is book four.
FIve.
This is the sixth.
This is book seven.
Eighth and last.yanush13@vm1:~$ sed '/book/ p' examples/books
Book one.
The second book.
The second book.
The third.
THis is book four.
THis is book four.
FIve.
This is the sixth.
This is book seven.
This is book seven.
Eighth and last.yanush13@vm1:~$ sed -n '/book/ p' examples/books
The second book.
THis is book four.
This is book seven.
yanush13@vm1:~$ sed -n '2,5 p' examples/books
The second book.
The third.
THis is book four.
FIve.
yanush13@vm1:~$ echo "2,5 p" >examples/records
yanush13@vm1:~$ sed -n -f examples/records examples/books
The second book.
The third.
THis is book four.
FIve.
yanush13@vm1:~$ cat >appends
3 a\
My favorite book.yanush13@vm1:~$ sed -f appends examples/books
Book one.
The second book.
The third.
My favorite book.
THis is book four.
FIve.
This is the sixth.
This is book seven.
Eighth and last.yanush13@vm1:~$ mv appends examples/
yanush13@vm1:~$ cat >examples/insert
/This/ i\
SKARBONKA.yanush13@vm1:~$ sed -f examples/insert examples/books
Book one.
The second book.
The third.
THis is book four.
FIve.
SKARBONKA.
This is the sixth.
SKARBONKA.
This is book seven.
Eighth and last.yanush13@vm1:~$ sed -n 's/book/novel/ p' examples/books
The second novel.
THis is novel four.
This is novel seven.
yanush13@vm1:~$ exit
exit

Script done on 2021-09-14 15:01:00+00:00 [COMMAND_EXIT_CODE="0"]
```
