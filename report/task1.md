# Задание 1

Журнал

```
Script started on 2021-09-14 14:19:06+00:00 [TERM="tmux-256color" TTY="/dev/pts/0" COLUMNS="119" LINES="32"]
yanush13@vm1:~$ cat >packages1.txt
amanda
galeon
metacity
mozilla
postgresql
procinfo
rpmfind
squidyanush13@vm1:~$ cat >packages2.txt
anaconda
openssh
gnome-core
samba
sendmail
xscreensaveryanush13@vm1:~$ cat packages1.txt
amanda
galeon
metacity
mozilla
postgresql
procinfo
rpmfind
squidyanush13@vm1:~$ cat packages1.txt >packages1.ca
yanush13@vm1:~$ cat packages2.txt >>packages1.catfil
8c8,1313@vm1:~$ diff packages1.txt packages1.catfile
< squid
\ No newline at end of file
---
> squidanaconda
> openssh
> gnome-core
> samba
> sendmail
> xscreensaver
\ No newline at end of file
yanush13@vm1:~$ tr 'aeiou' 'AEIOU' > trfile.txt
This time, when text is typed at the keyboard,
it is not echoed back to the screen with the tranlations made.
Instead, it is redirected to the file trfile.txt.
yanush13@vm1:~$ ls -l trfile.txt
-rw-rw-r-- 1 yanush13 yanush13 160 Sep 14 14:23 trfile.txt
yanush13@vm1:~$ cat trfile.txt
ThIs tImE, whEn tExt Is typEd At thE kEybOArd,
It Is nOt EchOEd bAck tO thE scrEEn wIth thE trAnlAtIOns mAdE.
InstEAd, It Is rEdIrEctEd tO thE fIlE trfIlE.txt.
yanush13@vm1:~$ cat < packages1.txt
amanda
galeon
metacity
mozilla
postgresql
procinfo
rpmfind
ackages1.trfile
yanush13@vm1:~$ cat packages1.trfile
AmAndA
gAlEOn
mEtAcIty
mOzIllA
pOstgrEsql
prOcInfO
rpmfInd
sqUIdyanush13@vm1:~$ exit
exit
```
