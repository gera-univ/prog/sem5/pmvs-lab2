# Задание 6

Журнал

```
Script started on 2021-09-14 15:37:40+00:00 [TERM="tmux-256color" TTY="/dev/pts/0" COLUMNS="65" LINES="32"]
yanush13@vm1:~$ alias lr='ls -lt'
yanush13@vm1:~$ lr > file_list.txt
yanush13@vm1:~$ tail -n 7 file_list.txt
drwxrwxr-x 5 yanush13 yanush13 4096 Sep 14 14:26 labrabota2gruppa13-jiffygist
-rw-rw-r-- 1 yanush13 yanush13   64 Sep 14 14:24 packages1.trfile
-rw-rw-r-- 1 yanush13 yanush13  160 Sep 14 14:23 trfile.txt
-rw-rw-r-- 1 yanush13 yanush13  119 Sep 14 14:21 packages1.catfile
-rw-rw-r-- 1 yanush13 yanush13   55 Sep 14 14:20 packages2.txt
-rw-rw-r-- 1 yanush13 yanush13   64 Sep 14 14:20 packages1.txt
-rwxrwxr-x 1 yanush13 yanush13   56 Sep 14 14:18 startlog.sh
yanush13@vm1:~$ exit
exit

Script done on 2021-09-14 15:38:00+00:00 [COMMAND_EXIT_CODE="0"]
```

```
Script started on 2021-09-14 18:50:41+00:00 [TERM="tmux-256color" TTY="/dev/pts/0" COLUMNS="159" LINES="43"]
yanush13@vm1:~$ cat examples/123numbers
# hello
123numbers
numbers123
yanush13@vm1:~$ sed -n '/^\(#\|[0-9]\).*/p' examples/123numbers
# hello
123numbers
yanush13@vm1:~$ exit
exit

Script done on 2021-09-14 18:50:51+00:00 [COMMAND_EXIT_CODE="0"]
```

```
Script started on 2021-09-14 21:32:35+00:00 [TERM="tmux-256color" TTY="/dev/pts/0" COLUMNS="159" LINES="43"]
yanush13@vm1:~$ ~/labrabota2gruppa13-jiffygist/examples/dirsize.sh /usr/bin
116005353
yanush13@vm1:~$ du -sb /usr/bin # для сравнения
112215242	/usr/bin
yanush13@vm1:~$ exit
exit

Script done on 2021-09-14 21:33:42+00:00 [COMMAND_EXIT_CODE="0"]
```
