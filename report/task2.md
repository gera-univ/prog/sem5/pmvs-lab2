# Задание 2

Журнал

```
Script started on 2021-09-14 14:38:46+00:00 [TERM="tmux-256color" TTY="/dev/pts/0" COLUMNS="53" LINES="32"]
yanush13@vm1:~$ mkdir examples
yanush13@vm1:~$ pwd
/home/yanush13
yanush13@vm1:~$ ls -la > examples/log.txt
yanush13@vm1:~$ awk '/bash/ {print}' examples/*
-rw------- 1 yanush13 yanush13 6925 Sep 14 14:38 .bash_history
-rw-r--r-- 1 yanush13 yanush13  220 Mar 19 16:02 .bash_logout
-rw-r--r-- 1 yanush13 yanush13 3771 Mar 19 16:02 .bashrc
yanush13@vm1:~$ awk '/config/ {print}' examples/*
drwx------ 3 yanush13 yanush13 4096 Sep  5 16:03 .config
-rw-rw-r-- 1 yanush13 yanush13   53 Sep 14 14:27 .
gawkapi.h        mpath_cmd.h      sudo_plugin.h
iproute2/        mpath_persist.h  xfs/
libdmmp/         reglib/
yanush13@vm1:~$ awk '/QUIT|SETDATE/' /usr/include/*
awk: cmd. line:1: warning: command line argument `/usr/include/iproute2' is a directory: skipped
awk: cmd. line:1: warning: command line argument `/usr/include/libdmmp' is a directory: skipped
awk: cmd. line:1: warning: command line argument `/usr/include/reglib' is a directory: skipped
awk: cmd. line:1: warning: command line argument `/usr/include/xfs' is a directory: skipped
yanush13@vm1:~$ awk '/QUIT|SETDATE/' /usr/include/*^C
yanush13@vm1:~$ # скопировал protocols с хоста
yanush13@vm1:~$ ls
examples                      startlog.sh
labrabota2gruppa13-jiffygist  task1Yanush13u
packages1.catfile             task2Yanush13u
packages1.trfile              timelog1Yanush13u
packages1.txt                 timelog2Yanush13u
packages2.txt                 trfile.txt
protocols
yanush13@vm1:~$ # скопировал protocols с хоста^C
yanush13@vm1:~$ awk '/QUIT|SETDATE/' protocols/
awk: warning: command line argument `protocols/' is a directory: skipped
yanush13@vm1:~$ awk '/QUIT|SETDATE/' protocols/*
#define	TSP_QUIT		13	/* reject candidature if master is up */
#define	TSP_SETDATE		22	/* New from date command */
#define	TSP_SETDATEREQ		23	/* New remote for above */
  "SLAVEUP", "ELECTION", "ACCEPT", "REFUSE", "CONFLICT", "RESOLVE", "QUIT",
  "TEST", "SETDATE", "SETDATEREQ", "LOOP" };
yanush13@vm1:~$ cat >myfile
This is a test.
This is the second test.
This is the third test.
This is the fourth test.
yanush13@vm1:~$ cat myfile
This is a test.
This is the second test.
This is the third test.
This is the fourth test.
yanush13@vm1:~$ awk '{print $1}' myfile
This
This
This
This
yanush13@vm1:~$ awk '{print $2}' myfile
is
is
is
is
yanush13@vm1:~$ awk '{print $3}' myfile
a
the
the
the
yanush13@vm1:~$ awk '/foo/ { print toupper($0); }'
This line contains bar.
This line contains foo.
THIS LINE CONTAINS FOO.
^C
yanush13@vm1:~$ cat >examples/list_students
Бокун Адам Антониевич
Варфоломеев Алексей Сергеевич
Врублевская Екатерина Александровна
Гордей Павел Дмитриевич
Доскоч Роман Дмитриевич
Ермолаева Екатерина Александровна
Мойсейчик Елизавета Сергеевна
Пархоменко Владимир Александрович
Петров Андрей Александрович
Протасеня Дмитрий Олегович
Семенович Дмитрий Анатольевич
Януш Герман Сергеевич
Ясинецкий Алексей Вячеславович

yanush13@vm1:~$ awk '{print $1,$2,$3}' examples/list_students
Бокун Адам Антониевич
Варфоломеев Алексей Сергеевич
Врублевская Екатерина Александровна
Гордей Павел Дмитриевич
Доскоч Роман Дмитриевич
Ермолаева Екатерина Александровна
Мойсейчик Елизавета Сергеевна
Пархоменко Владимир Александрович
Петров Андрей Александрович
Протасеня Дмитрий Олегович
Семенович Дмитрий Анатольевич
Януш Герман Сергеевич
Ясинецкий Алексей Вячеславович

yanush13@vm1:~$ awk '/Дми/ {print $0}' examples/list_students
Гордей Павел Дмитриевич
Доскоч Роман Дмитриевич
Протасеня Дмитрий Олегович
Семенович Дмитрий Анатольевич
yanush13@vm1:~$ awk '($2=="Дмитрий")' examplesxamples/list_students
Варфоломеев Алексей Сергеевич                elist_students
Врублевская Екатерина Александровна {print}' list_students
Ермолаева Екатерина Александровна5) {print}' list_students
Мойсейчик Елизавета Сергеевна
Пархоменко Владимир Александрович
Петров Андрей Александрович
Протасеня Дмитрий Олегович
Семенович Дмитрий Анатольевич
Януш Герман Сергеевич
yanush13@vm1:~$ й Вячеславович
yanush13@vm1:~$ awk '{print NR, $0}' examples/list_students
1 Бокун Адам Антониевичprint NR, $0}' examples/list_students
2 Варфоломеев Алексей Сергеевич
3 Врублевская Екатерина Александровна
4 Гордей Павел Дмитриевич
5 Доскоч Роман Дмитриевич
6 Ермолаева Екатерина Александровна
7 Мойсейчик Елизавета Сергеевна
8 Пархоменко Владимир Александрович
9 Петров Андрей Александрович
10 Протасеня Дмитрий Олегович
11 Семенович Дмитрий Анатольевич
12 Януш Герман Сергеевич
13 Ясинецкий Алексей Вячеславович
14
ist_students ~$ awk '{print toupper ($2)}' examples/l
АДАМ
АЛЕКСЕЙ
ЕКАТЕРИНА
ПАВЕЛ
yanush13@vm1:~$
yanush13@vm1:~$           = /Протасеня/)' examples/list
Бокун Адам Антониевич    ! /Протасеня/)' examples/list__
Варфоломеев Алексей Сергеевичотасеня/)' examples/list_
Врублевская Екатерина Александровнаня/)' examples/list_students
Гордей Павел Дмитриевич($1 ~ /Протасеня/) examples/lis
Доскоч Роман Дмитриевич ($1 ~ /Протасеня/) examples/li
Ермолаева Екатерина Александровнаротасеня/) examples/l
Мойсейчик Елизавета Сергеевна ~ /Протасеня/) examples/
Пархоменко Владимир Александрович/Протасеня/) examples/li
Петров Андрей Александрович ~ /Протасеня/) examples/litu_stdents
Протасеня Дмитрий Олегович /Протасеня/) examples/list_s
Семенович Дмитрий Анатольевичотасеня/) examples/list_students
yanush13@vm1:~$ еевич
yanush13@vm1:~$ й Вячес0 != /Протасеня/)' examples/list
Бокун Адам Антониевич   != /Протасеня/)' examples/list
Варфоломеев Алексей СергеевичПротасеня/)' examples/list_students
Врублевская Екатерина Александровна
Гордей Павел Дмитриевич
Доскоч Роман Дмитриевич
Ермолаева Екатерина Александровна
Мойсейчик Елизавета Сергеевна
Пархоменко Владимир Александрович
Петров Андрей Александрович
Протасеня Дмитрий Олегович
Семенович Дмитрий Анатольевич
yanush13@vm1:~$ еевич
yanush13@vm1:~$ й Вячеслав~ /Протасеня/)' examples/list
Бокун Адам Антониевич      /Протасеня/)' examples/list
Варфоломеев Алексей СергеевичПротасеня/)' examples/list_students
Врублевская Екатерина Александровна
Гордей Павел Дмитриевич
Доскоч Роман Дмитриевич
Ермолаева Екатерина Александровна
Мойсейчик Елизавета Сергеевна
Пархоменко Владимир Александрович
Петров Андрей Александрович
Семенович Дмитрий Анатольевич
Януш Герман Сергеевич
Ясинецкий Алексей Вячеславович

yanush13@vm1:~$ # ура!
yanush13@vm1:~$ cat >examples/colours.csv
apple,red,4
banana,yellow,6
strawberry,red,3
grape,purple,10
apple,green,8
plum,purple,2
kiwi,brown,4
potato,brown,9
pineapple,yellow,5                               examples/colours.csv > examples/output.txt
yanush13@vm1:~$ cat examples/output.txt
grape purple
apple green
potato brown
yanush13@vm1:~$ exit
exit

Script done on 2021-09-14 14:54:28+00:00 [COMMAND_EXIT_CODE="0"]
```
