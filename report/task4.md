# Задание 4

Журнал

```
Script started on 2021-09-14 15:03:51+00:00 [TERM="tmux-256color" TTY="/dev/pts/0" COLUMNS="65" LINES="32"]
yanush13@vm1:~$ a=879
yanush13@vm1:~$ echo "The value of \"a\" is $a."
The value of "a" is 879.
yanush13@vm1:~$ export full_name="John Lee"
yanush13@vm1:~$ echo $full_name
John Lee
yanush13@vm1:~$ export short_name="John"
yanush13@vm1:~$ echo $short_name
John
yanush13@vm1:~$ export short_name
yanush13@vm1:~$ echo $short_name
John
yanush13@vm1:~$ export foo=""
yanush13@vm1:~$ echo ${foo:~one}

yanush13@vm1:~$ echo $foo

yanush13@vm1:~$ echo ${foo:-one}
one
yanush13@vm1:~$ echo $foo

yanush13@vm1:~$ echo ${foo:=one}
one
yanush13@vm1:~$ echo $foo
one
yanush13@vm1:~$ export foo="this is a test"
yanush13@vm1:~$ echo $foo
yanush13@vm1:~$ vim ~/.bashrc
yanush13@vm1:~$ echo $PS1
${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$
yanush13@vm1:~$ PS1='Linux -> '
Linux -> PS1='\h $ '
vm1 $ exit
exit

Script done on 2021-09-14 15:07:49+00:00 [COMMAND_EXIT_CODE="0"]
```

```
Script started on 2021-09-14 15:08:05+00:00 [TERM="tmux-256color" TTY="/dev/pts/0" COLUMNS="65" LINES="32"]
yanush13@vm1:~$ echo sp{e1,il,a1}l
spe1l spill spa1l
yanush13@vm1:~$ cat ~/ee
cat: /home/yanush13/ee: No such file or directory
yanush13@vm1:~$ cat ~/examples/
appends        colours.csv    list_students  output.txt
books          insert         log.txt        records
yanush13@vm1:~$ cat ~/examples/books
Book one.
The second book.
The third.
THis is book four.
FIve.
This is the sixth.
This is book seven.
Eighth and last.yanush13@vm1:~$
yanush13@vm1:~$ echo $SHELL
/bin/bash
yanush13@vm1:~$ echo $(date)
Tue Sep 14 03:09:11 PM UTC 2021
yanush13@vm1:~$ X=5
yanush13@vm1:~$ Y=3
yanush13@vm1:~$ echo Area: $[$X * $Y]
Area: 15
yanush13@vm1:~$ echo Your cost: \$5,00
Your cost: $5,00
yanush13@vm1:~$ echo '$date'
$date
yanush13@vm1:~$ exit
exit

Script done on 2021-09-14 15:09:52+00:00 [COMMAND_EXIT_CODE="0"]
```

```
Script started on 2021-09-14 15:03:51+00:00 [TERM="tmux-256color" TTY="/dev/pts/0" COLUMNS="65" LINES="32"]
yanush13@vm1:~$ a=879
yanush13@vm1:~$ echo "The value of \"a\" is $a."
The value of "a" is 879.
yanush13@vm1:~$ export full_name="John Lee"
yanush13@vm1:~$ echo $full_name
John Lee
yanush13@vm1:~$ export short_name="John"
yanush13@vm1:~$ echo $short_name
John
yanush13@vm1:~$ export short_name
yanush13@vm1:~$ echo $short_name
John
yanush13@vm1:~$ export foo=""
yanush13@vm1:~$ echo ${foo:~one}

yanush13@vm1:~$ echo $foo

yanush13@vm1:~$ echo ${foo:-one}
one
yanush13@vm1:~$ echo $foo

yanush13@vm1:~$ echo ${foo:=one}
one
yanush13@vm1:~$ echo $foo
one
yanush13@vm1:~$ export foo="this is a test"
yanush13@vm1:~$ echo $foo
yanush13@vm1:~$ vim ~/.bashrc
yanush13@vm1:~$ echo $PS1
${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$
yanush13@vm1:~$ PS1='Linux -> '
Linux -> PS1='\h $ '
vm1 $ exit
exit

Script done on 2021-09-14 15:07:49+00:00 [COMMAND_EXIT_CODE="0"]
```

Для 4.3 script испортился
