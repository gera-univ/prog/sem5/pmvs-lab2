#!/usr/bin/awk

BEGIN {
	getline
	min=max=$3
}

{
	if ($3 > max) {
		max = $3
		maxline = $0
	}
	if ($3 < min) {
		min = $3
		minline = $0
	}
}

function printarray(array) {
	size = length(array)
	for (i = 1; i < size; ++i) {
		printf "%s%c",array[i],(i==size-1)?"\n":"\t"
	}
}

END {
	nmin = split(minline,minarray)
	nmax = split(maxline,maxarray)
	minarray[0] = toupper(minarray[0])
	maxarray[0] = toupper(maxarray[0])
	printarray(minarray)
	printarray(maxarray)
}
