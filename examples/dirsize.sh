#!/bin/bash

#>&2 echo arg:$1
path=$(pwd)
SCRIPTPATH=$(realpath $0)
target="$( echo $1 | xargs )"
#>&2 echo going to $target from $(pwd)
if [ -z "$target" ]
then
	echo 0
	exit
fi
cd "$target"
if [ $? != 0 ] 
then
	echo 0
	exit
fi
for d in {.*/,*/}
do
	if [ "$d" == "./" ] || [ "$d" == "../" ] || [ "$d" == "*/" ]
	then
		continue
	else
		is_link=$(ls -l "$d" | sed -n '2 p' | awk '{ printf "%c",$1 }')
		>&2 echo avoiding symlink "$(pwd)/$d"
		if [ $is_link == "l" ]
		then
			continue
		fi
	fi
	#>&2 echo $d
	other_dirsize=`$SCRIPTPATH \""$d"\"`
	#>&2 echo $dirsize
done
dirsize=$(ls -la | sed -n '/^-.*/p' | awk '{sum+=$5} END{print sum}')
if [ ! -z $other_dirsize ]
then
	dirsize=$[ $dirsize + $other_dirsize ]
fi
echo $dirsize
cd $path
