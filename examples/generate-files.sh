#!/bin/bash

for i in {0001..1000}
do
	printf %100s | sed "s/ /${i} /g" > /tmp/file_$i.txt
done

odds=$(seq 1 2 1000)
evens=$(seq 2 2 1000)
odd_files=$(printf "/tmp/file_%04d.txt\n" ${odds[@]})
even_files=$(printf "/tmp/file_%04d.txt\n" ${evens[@]})

cat $odd_files > /tmp/odd.txt
cat $even_files > /tmp/even.txt
